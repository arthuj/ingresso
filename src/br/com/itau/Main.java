package br.com.itau;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Ingresso ingresso = new Ingresso(100);
        VIP vip = new VIP(ingresso.getValor(),15);
        Normal normal = new Normal(ingresso.getValor());

        vip.imprimeValor();
        normal.imprimeValor();
    }
}
