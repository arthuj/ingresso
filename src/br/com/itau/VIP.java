package br.com.itau;

public class VIP extends Ingresso {

    protected double valorAdicional;

    public VIP(double valor, double valorAdicional) {
        super(valor);
        this.valorAdicional = valorAdicional;
        this.valor += valorAdicional;
    }

    public double getValorVIP()
    {
        return this.valor;
    }

    public double getValorAdicional() {
        return valorAdicional;
    }

    public void setValorAdicional(double valorAdicional) {
        this.valorAdicional = valorAdicional;
    }
}
